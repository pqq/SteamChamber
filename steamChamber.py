
# --------------------------------------------------------------------------
# file          : steamChamber.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : april 2014
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------

import  re
import  os
import  stat
import  json
import  time
import  uuid
import  email
import  codecs
import  random
import  urllib
import  shutil
import  smtplib
import  difflib
import  datetime
from    xml.dom import minidom
from    email.mime.text import MIMEText
import  csvMerger


# ------------------------------------------------------
# data
# ------------------------------------------------------

# specify the correct environment
environment             = "dev"
#environment            = "prod_ip"
#environment            = "prod"

steam_api_key           = "E884AFF22304FD5679EAF6649DA418F8"
inactive_days_limit     = 90                                                                                                                    # the number of days, since last data entry is logged, before players are set to inactive
request_limit           = 5                                                                                                                     # no of requests that can be done, before age limit is reached and counter is reset
sc_playername_ad        = ["steamchamber.com", "st&#603;amchamber.com"]                                                                         # valid ad strings
sc_groupid_ad           = [103582791437625375, 103582791438111254]                                                                              # SteamChamber private group, SteamChamber public group
players_file            = "database/players.json"
os_base_path            = ""
url_base_path           = ""
os_db_relative_path     = "database/"
os_json_relative_path   = "jsonOut/"
message_sender_name     = "SteamChamber.com"
message_sender_email    = "info@steamchamber.com"
message_footer          = "\n \nwww.SteamChamber.com"
smtp_server             = ""
smtp_port               = ""


# ------------------------------------------------------
# functions
# ------------------------------------------------------

# isEnv() needs to be declared first
def isEnv(name):
    """
    environment checker (returns true or false, based on input and what environment that is set)

    input values:
     - dev
     - prod
    """

    if name == environment:
        return True
    else:
        return False

# needs to be declared directly after isEnv()
if isEnv("dev"):
    os_base_path    = "C:/Users/klevstul/_miscellaneous/Dropbox/Miscellaneous/projects/_gitHub/SteamChamber/"
    url_base_path   = "http://127.0.0.1:8080/"
    smtp_server     = "127.0.0.1"
    smtp_port       = 1025
elif isEnv("prod_ip"):
    os_base_path    = "/var/www/www.steamchamber.com/"
    url_base_path   = "http://178.62.247.188/"
elif isEnv("prod"):
    os_base_path    = "/var/www/www.steamchamber.com/"
    url_base_path   = "http://steamchamber.com/"
    smtp_server     = "127.0.0.1"
    smtp_port       = 25
# / end of isEnv


def backupPlayers():
    """
    create a backup of the players database
    """

    timestamp = time.time()
    shutil.copyfile(os_base_path + players_file, os_base_path + players_file + "." + str(int(timestamp)) + ".backup")


def backupStatistics(steamid):
    """
    backup statistics db file, for player with given steam id
    """

    fname = os_base_path + "database/" + steamid + ".csv"

    if os.path.isfile(fname):
        timestamp = time.time()
        shutil.copyfile(fname, fname + "." + str(int(timestamp)) + ".backup")


def debug(message):
    """
    print debug info
    """

    if isEnv("dev"):
        print "] " + message


def deleteRequestKey(steamid):
    """
    delete request key file, for player with given steam id
    """

    fname = os_base_path + "database/" + steamid + ".key"

    if os.path.isfile(fname):
        os.remove(fname)


def deleteStatistics(steamid):
    """
    delete statistics db file, for player with given steam id
    """

    fname = os_base_path + "database/" + steamid + ".csv"

    if os.path.isfile(fname):
        os.remove(fname)


def error(message, exit=True):
    """
    print error message, and exit
    """

    print "ERROR: " + message
    if exit:
        raise SystemExit


def fileAgeInSeconds(pathname):
    """
    return age of file, in seconds
    """

    # ref: http://stackoverflow.com/questions/6879364/print-file-age-in-seconds-using-python
    # ref: https://docs.python.org/2/library/stat.html

    try:
        return time.time() - os.stat(pathname)[stat.ST_CTIME]
    except:
        return 9999999999


def getAllDbCsvFiles():
    """
    get all csv files located in the database directory
    """

    files_array = [ f for f in os.listdir(os_db_relative_path) if os.path.isfile(os.path.join(os_db_relative_path,f)) and f.endswith(".csv") ]

    return files_array


def getAllExtrasFiles():
    """
    get all extras (extra statistics) json generated statistics files
    """

    files_array = [f for f in os.listdir(os_json_relative_path) if os.path.isfile(os.path.join(os_json_relative_path,f)) and f.endswith("_extras.json")]

    return files_array


def getAllJsonStatFiles():
    """
    get all json generated statistics files
    """

    files_array = [f for f in os.listdir(os_json_relative_path) if os.path.isfile(os.path.join(os_json_relative_path,f)) and os.path.splitext(f)[0].isdigit()]

    return files_array


def getDaysSinceLastEntry(steamid):
    """
    return the number of days, since the last entry was stored
    """

    try:
        file_lastline = ""
        with open("database/"+steamid+".csv", "rb") as fh:
            offs = -100
            while True:
                fh.seek(offs, 2)
                lines = fh.readlines()
                if len(lines)>1:
                    file_lastline = lines[-1]
                    break
                offs *= 2
        fh.close()

        date_string = file_lastline[:10]
    except:
        date_string = "3001-01-01"

    # zeros only will lead to value error (sc-185)
    if date_string == "0000-00-00":
        date_string = "3001-01-01"

    try:
        date_object = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    except:
        date_object = datetime.datetime.strptime("3001-01-01", "%Y-%m-%d")

    delta       = datetime.datetime.now() - date_object

    return delta.days


def getExtrasForPlayer(steamid):
    """
    request local database, ask for player's extra data / extra statistics, data is json formatted, but loaded and returned as text
    """

    path_local  = os_base_path + "jsonOut/" + steamid + "_extras.json"

    with codecs.open(path_local, "r", "utf-8-sig") as outfile:
        return_json = json.loads(outfile.read())
    outfile.close()

    return return_json


def getPlayer(steamid):
    """
    return a player, given steam id, as json / dict
    """

    players_all     = getPlayers()
    player_found    = {}

    for player_group in players_all:
        for player in players_all[player_group]:
            if player["steamid"] == steamid:
                player_found = player

                # we might not have a realname entry
                if not "realname" in player_found:
                    player_found["realname"] = ""

                break

    return player_found


def getPlayers():
    """
    return all players, as json / dict
    """

    f               = open(os_base_path + players_file, "r")
    players_content = json.load(f)
    players         = players_content
    f.close()
    return players


def getPlayerSteamchamberStatus(steamid):
    """
    request local database, ask for player's sc status, data is returned as json
    """

    # initiate "return player" object
    player_found                = False
    return_player               = {}
    return_player["steamid"]    = steamid

    player                      = getPlayer(steamid)

    if player:
        player_found                    = True
        return_player["subscription"]   = player["subscription"]

    return_player["is_subscriber"]      = player_found
    return_json                         = return_player

    return return_json


def getQuote():
    """
    request local database, ask for quote, data is returned as json
    """

    path_local = os_base_path + "database/quotes.txt"
    local_file = open(path_local, "r")
    line = next(local_file)
    for num, aline in enumerate(local_file):
        if random.randrange(num + 2): continue
        return_text = aline
    local_file.close()

    return_json             = {}
    return_json["quote"]    = return_text.rstrip("\n")

    return return_json


def getStatForPlayerAsCsv(steamid):
    """
    request local database, ask for player stat, return data as csv
    """

    path_local  = os_base_path + "database/" + steamid + ".csv"
    local_file  = open(path_local, "r")
    return_text = local_file.read()
    local_file.close()

    return return_text


def getStatForPlayerAsJson(steamid):
    """
    request local database, ask for player stat, data is json formatted, but loaded and returned as text
    """

    path_local  = os_base_path + "jsonOut/" + steamid + ".json"
    with codecs.open(path_local, "r", "utf-8-sig") as outfile:
        return_json = json.loads(outfile.read())
    outfile.close()

    return return_json


def getSubscription(steamid):
    """
    return subscription type for player with given steam id
    """

    players         = getPlayers()
    subscription    = ""

    for player_group in players:
        for player in players[player_group]:
            if int(player["steamid"]) == int(steamid):
                subscription = player["subscription"]

    return subscription


def getUniqueRequestKey():
    """
    generate and return a unique key, for requests
    """

    return str(uuid.uuid1()).replace('-','')


def getWallOfAwesomeness():
    """
    request local database, ask for wall of awesomeness results, data is returned as json
    """

    path_local  = os_base_path + "jsonOut/wallOfAwesomeness.json"
    local_file  = open(path_local, "r")
    return_json = json.loads(local_file.read())
    local_file.close()

    return return_json


def isVerifiedSubscription(player):
    """
    checks if player is valid for a simple subscription
    """

    valid_personaname   = False
    valid_primary_group = True                  # not checking for group (sc-253)
    message             = ""

    # load player data from steamchamber
    player_sc           = getPlayer(player["steamid"])

    # for free subscriptions player name must include "steamchamber.com", and primary group must be the steamchamber group.
    if ( player_sc["subscription"] == "free"):
        for ad_string in sc_playername_ad:
            if ad_string in player["personaname"].encode("ascii", "xmlcharrefreplace").lower():
                valid_personaname = True
                #debug("ad found in personaname")

        for group in player["groups"]:
            if int(group["isPrimary"]) == 1:
                for valid_group in sc_groupid_ad:
                    if valid_group == int(group["groupID64"]):
                        valid_primary_group = True
                        #debug("sc group set as primary group")

        if not valid_personaname:
            message = "ad not found in personaname ("+player["personaname"].encode("ascii", "xmlcharrefreplace")+")"
            #debug(message)

        if not valid_primary_group:
            if (message):
                message += ", "
            message += "sc group not set as primary group"
            #debug(message.split(", ")[1])

    # for full and sponsored subscriptions there are no requirements
    elif (player_sc["subscription"] == "full" or player_sc["subscription"] == "sponsored"):
        valid_personaname   = True
        valid_primary_group = True

    # inactive subscriptions
    elif ("inactive-" in player_sc["subscription"]):
        message = "subscription is inactive";

    # paused subscriptions
    elif ("pause-" in player_sc["subscription"]):
        message = "subscription is paused";

    # unknown subscription
    else:
        message = "subscription type unknown";

    if valid_personaname and valid_primary_group:
        return (True, "ok")
    else:
        return (False, message)


def isRequestsAvailable(steamid):
    """
    if more requests available: increase requests with one and return true, otherwise: return false
    """

    age_limit       = 60 * 60 * 24                                                                                                          # in seconds
    return_value    = False
    reqc_file       = os_base_path + "database/" + steamid + ".reqc"

    # check if request counter (reqc) file is outdated. if it is, delete it (which reset counter)
    age = fileAgeInSeconds(reqc_file)
    if age >= age_limit:
        try:
            os.remove(reqc_file)
            time.sleep(15)                                                                                                                  # on windows, if you do not wait 15 sec in between deleting a file, and creating a file with the same name, the creation date do not change, hence this sleep
        except:
            pass

    # check the counter
    try:
        f = open(reqc_file, "r")
        requests = int(f.read().strip())
        f.close()

        if requests < request_limit:
            requests += 1

            f = open(reqc_file, "w")
            f.write(str(requests))
            f.close

            return_value = True
        else:
            return_value = False

    except:
        f = open(reqc_file, "w")
        f.write("1")
        f.close

        return_value = True

    return return_value


def isSubscriber(steamid):
    """
    check if a player with given steam id do exist in the player database
    """

    player = getPlayer(steamid)

    if player:
        return True
    else:
        return False


def isValidRequestKey(steamid, key):
    """
    check key against stored key, for player with given steam id
    """

    validKey = False

    try:
        f = open(os_base_path + "database/"+steamid+".key", "r")
        keyOnOs = f.read().strip()
        f.close()

        if keyOnOs == key:
            validKey = True
    except:
        validKey = False

    return validKey


def removeElementsFromDataStructure(obj, fields_list):
    """
    remove elements with names in "fields_list" from datastructure
    """

    # reference:
    # http://stackoverflow.com/questions/20558699/python-how-recursively-remove-none-values-from-a-nested-data-structure-lists-a
    if isinstance(obj, (list, tuple, set)):
        return type(obj)(removeElementsFromDataStructure(x, fields_list) for x in obj if x not in fields_list)
    elif isinstance(obj, dict):
        return type(obj)((removeElementsFromDataStructure(k, fields_list), removeElementsFromDataStructure(v, fields_list)) for k, v in obj.items() if k not in fields_list)
    else:
        return obj


def removePlayerFromArray(playersArray, steamid):
    """
    remove player from an array of players
    """

    players_array_new = []

    for player in playersArray:
        if player["steamid"] == steamid:
            continue
        else:
            players_array_new.append(player)

    return players_array_new


def requestGetFriendList(steamid):
    """
    request steam or local database, ask for a player's list of friends, data is returned as json
    """

    def addSubscriberStatusAndName(subscribers, dictionary):
        """
        add extra player info to dictionary
        """
        for element in dictionary["friendslist"]["friends"]:
            # subscriber status
            element["is_subscriber"]        = element["steamid"] in subscribers

            # subscription type
            index_steamid                   = subscribers.find(element["steamid"])
            index_beginning_of_subscription = subscribers.find("[sc:subscription]", index_steamid)+17
            index_end_of_subscription       = subscribers.find("[sc:steamidNameSplit]", index_steamid)
            subscription                    = subscribers[index_beginning_of_subscription:index_end_of_subscription]

            # player name (persona name)
            index_beginning_of_player_name  = subscribers.find("[sc:steamidNameSplit]", index_steamid)+21
            index_end_of_player_entry       = subscribers.find("[sc:endOfPlayerEntry]", index_steamid)
            playerName                      = subscribers[index_beginning_of_player_name:index_end_of_player_entry]

            # add extra info to element in dict
            if index_steamid != -1:
                element["personaname"]      = playerName
                element["subscription"]     = subscription

    # first, we build a list of sc subscribers (sc subscriber status to be added to friends list)
    subscribers = ""

    players_json = getPlayers()

    for player in players_json["players"]:
        subscribers += player["steamid"] + "[sc:subscription]" + player["subscription"] + "[sc:steamidNameSplit]" + player["personaname"] + "[sc:endOfPlayerEntry]"

    #print subscribers.encode("utf-8")

    # example url:
    # http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=E884AFF22304FD5679EAF6649DA418F8&steamid=76561197971126706&relationship=friend

    url_steam   = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=[steam_api_key]&steamid=[steamid]&relationship=friend"
    url_steam   = url_steam.replace("[steam_api_key]", str(steam_api_key))
    url_steam   = url_steam.replace("[steamid]", str(steamid))

    path_local  = os_base_path + "jsonOut/" + steamid + "_friends.json"

    # if local file do exist, we use that, otherwise we request the steam api, and afterwards save those data locally
    if os.path.isfile(path_local):
        local_file   = open(path_local, "r")
        return_json = json.loads(local_file.read())
        local_file.close()
        addSubscriberStatusAndName(subscribers, return_json)
    else:
        # get data from steam
        return_object   = urllib.urlopen(url_steam)
        return_json     = json.loads(return_object.read())
        local_file      = open(path_local, "w")
        local_file.write( json.dumps(return_json, encoding = "utf-8", indent = 4) )
        local_file.close()
        # we do not write subscriber status to file, as we want fresh sc status used for all requests, hence we add subscriber status after file is written
        addSubscriberStatusAndName(subscribers, return_json)

    return return_json


def requestGetItemsSchema():
    """
    request steam, ask for items schema, return data as json / dict
    """

    # example url:
    # https://api.steampowered.com/IEconItems_730/GetSchema/v2/?key=E884AFF22304FD5679EAF6649DA418F8

    url_steam   = "https://api.steampowered.com/IEconItems_730/GetSchema/v2/?key=[steam_api_key]"
    url_steam   = url_steam.replace("[steam_api_key]", str(steam_api_key))
    path_local  = os_base_path + "jsonOut/itemsSchema.json"

    # if local file do exist, we use that, otherwise we request the steam api, and save the data locally
    if os.path.isfile(path_local):
        local_file   = open(path_local, "r")
        return_json = json.loads(local_file.read())
        local_file.close()
    else:
        return_object   = urllib.urlopen(url_steam)
        return_json     = json.loads(return_object.read())
        local_file      = open(path_local, "w")
        local_file.write( json.dumps(return_json, encoding = "utf-8", indent = 4) )
        local_file.close()

    return return_json


def requestGetNewsForApp():
    """
    request steam, ask for cs news, return data as json / dict
    """

    url_steam   = "http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=730&count=3&maxlength=300&format=json"
    path_local  = os_base_path + "jsonOut/csgoNews.json"

    # if local file do exist, we use that, otherwise we request the steam api, and save the data locally
    if os.path.isfile(path_local):
        local_file   = open(path_local, "r")
        return_json = json.loads(local_file.read())
        local_file.close()
    else:
        return_object    = urllib.urlopen(url_steam)
        return_json     = json.loads(return_object.read())
        local_file       = open(path_local, "w")
        local_file.write( json.dumps(return_json, encoding = "utf-8", indent = 4) )
        local_file.close()

    return return_json


def requestGetPlayerSummaries(steamid):
    """
    request steam, ask for player profile info, return data as json / dict
    note: returns similar data as 'requestPlayerProfileInfo()', however here we're here using the steam api instead of the web / xml
    """

    # example url:
    # http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=E884AFF22304FD5679EAF6649DA418F8&steamids=76561197971126706

    url_steam   = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=[steam_api_key]&steamids=[steamid]"
    url_steam   = url_steam.replace("[steam_api_key]", str(steam_api_key))
    url_steam   = url_steam.replace("[steamid]", str(steamid))
    path_local  = os_base_path + "jsonOut/" + steamid + "_summaries.json"

    # if local file do exist, we use that, otherwise we request the steam api, and afterwards save those data locally
    if os.path.isfile(path_local):
        local_file  = open(path_local, "r")
        return_json = json.loads(local_file.read())
        local_file.close()
    else:
        return_object   = urllib.urlopen(url_steam)
        return_json     = json.loads(return_object.read())
        local_file      = open(path_local, "w")
        local_file.write( json.dumps(return_json, encoding = "utf-8", indent = 4) )
        local_file.close()

    return return_json


def requestGetUserStats(steamid, key):
    """
    request steam, ask for player statistics, for players with valid subscription
    """

    player_data                     = requestPlayerProfileInfo(steamid)
    (subscription_valid, message)   = isVerifiedSubscription(player_data)

    # subscription is valid
    if (subscription_valid):

        # check that request key is valid
        if isValidRequestKey(steamid, key):

            # check that number of requests is lower than the request limit
            if isRequestsAvailable(steamid):
                stats       = requestStatForPlayer(steamid, True)
                return_info = saveStatForPlayer(steamid, stats, True)
                return_text = return_info
            else:
                return_text = "{\"error\": \"request_limit_reached\"}"

        # invalid request key
        else:
            return_text = "{\"error\": \"invalid_request_key\"}"

    # subscription is not valid
    else:
        return_text = "{\"error\": \""+message+"\"}"

    return_json = json.loads(return_text)

    return return_json


def requestPlayerProfileInfo(steamid_or_profile_name):
    """
    request steam, ask from player data, returning a player dict - note: this method is using the xml web request method, not the steam api
    """

    steam_profile_url_using_name    = "http://steamcommunity.com/id/"+steamid_or_profile_name+"/?xml=1"
    steam_profile_url_using_id      = "http://steamcommunity.com/profiles/"+steamid_or_profile_name+"/?xml=1"

    # first we try to access profile using player name
    xml_str                 = urllib.urlopen(steam_profile_url_using_name).read()

    # ... if profile is not found, we try accessing the profile using id
    if xml_str.find("The specified profile could not be found") > 0:
        xml_str             = urllib.urlopen(steam_profile_url_using_id).read()

    personaname             = ""
    xmldoc                  = minidom.parseString(xml_str)
    steamid_elements        = xmldoc.getElementsByTagName("steamID64")
    personaname_elements    = xmldoc.getElementsByTagName("steamID")
    realname_elements       = xmldoc.getElementsByTagName("realname")
    avatar_elements         = xmldoc.getElementsByTagName("avatarIcon")
    privacy_state_elements  = xmldoc.getElementsByTagName("privacyState")
    groups_elements         = xmldoc.getElementsByTagName("groups")
    try:
        steamid             = steamid_elements[0].firstChild.nodeValue
        personaname         = personaname_elements[0].firstChild.nodeValue
        avatar              = avatar_elements[0].firstChild.nodeValue
        privacyState        = privacy_state_elements[0].firstChild.nodeValue
        try:
            realname        = realname_elements[0].firstChild.nodeValue
        except:
            realname        = ""
    except:
        steamid             = 0
        error(xml_str, False)

    # if we had problems loading the steam profile...
    if steamid == 0:
        player = {}

    # build a player dict
    else:

        #debug("steamChamber.requestPlayerProfileInfo() : get info for player " + personaname.encode("ascii", "xmlcharrefreplace"))

        player                  = {}
        player["steamid"]       = steamid
        player["personaname"]   = personaname
        player["avatar"]        = avatar
        player["realname"]      = realname
        player["privacyState"]  = privacyState
        player["groups"]        = []                # list of groups, to hold dicts with group info

        for groups in groups_elements:
            groupList = groups.getElementsByTagName("group")

            for group in groupList:
                isPrimary = group.getAttribute("isPrimary")
                groupID64 = group.getElementsByTagName("groupID64")[0].firstChild.nodeValue
                player["groups"].append({"groupID64": groupID64, "isPrimary": isPrimary})

    return player


def requestStatForPlayer(steamid, silent_mode = False):
    """
    request steam, ask for player statistics
    """

    url_get_user_stats_for_game = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=[steam_api_key]&steamid=[steamid]"
    url_get_user_stats_for_game = url_get_user_stats_for_game.replace("[steam_api_key]", str(steam_api_key))

    try:
        # fetch stat for current player
        url_get_user_stats_for_game = url_get_user_stats_for_game.replace("[steamid]", str(steamid))
        url_stats                   = urllib.urlopen(url_get_user_stats_for_game)
        json_result_stats           = json.loads(url_stats.read())

    except:
        if not silent_mode:
            print url_get_user_stats_for_game
            print "] unable to fetch data for player with steam id " + steamid + " (private profile? check url "+ url_get_user_stats_for_game +")"
        return []

    # extra check to ensure data correctness
    steamid_ = json_result_stats["playerstats"]["steamID"]
    if ( str(steamid) != str(steamid_) ):
        error("steamid mismatch : ["+str(steamid)+" != "+str(steamid_)+"]")

    if not silent_mode:
        print "\n] fetching statistics for player with steamid " + steamid + ""

    stats = json_result_stats["playerstats"]["stats"]

    return stats


def savePlayers(players):
    """
    save players dictionary to file
    """

    local_file = open(os_base_path + players_file, "w")
    local_file.write( json.dumps(players, encoding = "utf-8", indent = 4) )
    local_file.close()


def saveRequestKey(steamid, key):
    """
    create request key file, for player with given steam id, and store the key
    """

    local_file = open(os_base_path + "database/" + steamid + ".key", "w")
    local_file.write( key )
    local_file.close()


def saveStatForPlayer(steamid, stats, web_mode = False):
    """
    store statistics for player with given steam id
    """

    output_path     = os_base_path + "database/"
    heading         = ""
    line            = ""
    file_firstline  = ""
    file_lastline   = ""
    timestamp       = time.time()
    return_message  = ""

    # make sure we have some stats...
    if len(stats) > 0:

        for stat in stats:
            return_message += str(stat["name"]) + ": " + str(stat["value"]) + "\n"
            heading = heading   + "," + str(stat["name"])
            line    = line      + "," + str(stat["value"])

        # create heading and data line
        timestamp_formatted = datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M")
        heading             = "fetched_timestamp" + heading
        line                = timestamp_formatted + line

        # one csv file is kept for each player / steam id
        filename = output_path + str(steamid)+".csv"

        # if file exists on os from before...
        if os.path.isfile(filename):
            return_message += "] opening existing player file\n"

            # read first line (heading) and last line (last player data saved), ref: http://stackoverflow.com/questions/3346430/most-efficient-way-to-get-first-and-last-line-of-file-python
            with open(filename, "rb") as fh:
                file_firstline = next(fh)
                offs = -100
                while True:
                    fh.seek(offs, 2)
                    lines = fh.readlines()
                    if len(lines)>1:
                        file_lastline = lines[-1]
                        break
                    offs *= 2
            fh.close()

            # if the header has changed, merge new data with old data, after backing up old data
            if heading != file_firstline.rstrip():
                return_message += "] header has changed : \n[\ngenerated:\n"+heading+"\nfile:\n"+file_firstline+"]\n"

                # take a copy of the old file
                shutil.copyfile(filename, filename + "." + str(int(timestamp)) + ".backup")

                # write new content to a temporary file
                f = file(filename + ".tmp", "w")
                f.write( heading + "\n")
                f.write( line + "\n")
                f.close()

                # if the length of the header on file is longer than the generated header, we need to manually remove extra columns from the file
                # in these cases an error file is written to disk, and will be generated until this is fixed. note that no data is lost though,
                # if this is the case. however, a lot of backup files will be generated, as it looks like it is a new header every time data is
                # fetched from steam.
                if len(heading) < len(file_firstline.rstrip()):
                    return_message += "] header on file is longer than generated header. keep old header. add data line. write overflow file.\n"

                    s1              = heading
                    s2              = file_firstline.rstrip()
                    d               = difflib.Differ()
                    diff            = difflib.ndiff(s1, s2)
                    extra_string    = s2.replace("".join(difflib.restore(diff, 1)), "")

                    return_message += "] extra columns in header needs to manually be removed:\n" + extra_string + "\n"
                    f = file(filename + "." + str(int(timestamp)) + ".error.overflow", "w")
                    f.write( extra_string + "\n")
                    f.close()
                elif len(heading) > len(file_firstline.rstrip()):
                    s1 = heading
                    s2 = file_firstline.rstrip()
                    extra_string=difflib.unified_diff(s1, s2)
                    extra_string = "".join(extra_string)

                    return_message += "] header on file is shorter than generated header. new file to be generated.\n] diff details: \n" + extra_string
                else:
                    return_message += "] header on file has equal length as generated header, however the headers are still different.\n"

                # merge old format into new format, losing the old format
                csvMerger.merge(filename, filename+".tmp", filename)

                # delete tmp file
                os.remove(filename + ".tmp")

            # if we have written the same player data before, do not re-write it, note that the first element of the line is the date, this will change so we remove it before the match test
            elif not re.match(line.split(",",1)[1], file_lastline.split(",",1)[1]):
                return_message += "] appending new data line\n"
                f = file(filename, "a+")
                f.write( line + "\n")
                f.close()
            else:
                return_message += "] player data previously written\n"

        # if file don't exists, we create one...
        else:
            return_message += "] creating new file for player\n"
            f = file(filename, "w")
            f.write( heading + "\n")
            f.write( "0000-00-00 00:00," + line.split(",",1)[1] + "\n")                                                                     # set fetched date to unknown for the first entry, to avoid confusion (sc-21)
            f.close()

        if web_mode:
            statistics      = ""
            processing_info = ""
            final_status    = ""
            this_stat       = []

            lines = return_message.split("\n")

            for l in lines:
                # if we have a status line...
                if re.match("^]", l):
                    final_status = l
                    processing_info += l + " "

                # if we have a statistics line ...
                elif re.match("\w", l):
                    this_stat = l.split(":")
                    statistics += "\""+this_stat[0]+"\": " + this_stat[1] + ",\n"


            statistics      = statistics.rstrip()[:-1]                                                                                      # remove the final newline and then the final comma
            processing_info = processing_info.rstrip()
            final_status    = final_status[2:]
            return_message  = "{\n\"statistics\":\n{\n" + statistics + "\n},\n\"processing_info\": \""+processing_info+"\",\n\"final_status\": \""+final_status+"\"\n}"

        return return_message


def sendMessage(player, message_type, title, message):
    """
    send message to player (dict)
    """

    if message_type == "email" and "email" in player:
        #msg             = MIMEText(message)
        msg             = MIMEText(message + message_footer, "plain", "utf-8")
        msg["To"]       = email.utils.formataddr((player["personaname"].encode("ascii","ignore"), player["email"]))
        msg["From"]     = email.utils.formataddr((message_sender_name, message_sender_email))
        msg["Bcc"]      = email.utils.formataddr((message_sender_name, message_sender_email))
        msg["Subject"]  = title
        server          = smtplib.SMTP(smtp_server, smtp_port)

        if isEnv("dev"):
            # show communication with the server
            server.set_debuglevel(True)

        try:
            server.sendmail(message_sender_email, player["email"], msg.as_string())
        except:
            error("Could not send email", False)
        finally:
            server.quit()
    else:
        debug("no message sent")


def updatePlayerInfo(player):
    """
    given a player object, request new player info, and return a updated player object
    """

    steamid                     = player["steamid"]
    url_get_player_summaries    = url_base_path + "rpc.py?method=GetPlayerSummaries&steamid=" + steamid
    return_object               = urllib.urlopen(url_get_player_summaries)
    line                        = return_object.readline()
    lines                       = ""
    while line:
        lines   += line
        line    = return_object.readline()

    return_json             = json.loads(lines)
    player["personaname"]   = return_json["response"]["players"][0]["personaname"]

    try:
        player["realname"]  = return_json["response"]["players"][0]["realname"]
    except:
        player["realname"]  = ""

    try:
        player["avatar"]    = return_json["response"]["players"][0]["avatar"]
    except:
        player["avatar"]    = ""

    return player
