
# --------------------------------------------------------------------------
# file          : updatePlayerNames.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : march 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  json
import  datetime
import  steamChamber
from    urllib import urlopen


players             = steamChamber.getPlayers()
players_            = list(players["players"])                                                                                              # we need to make a copy of the list (http://henry.precheur.org/python/copy_list.html)
players_inactive_   = list(players["players_inactive"])


for player in players_:
    player = steamChamber.updatePlayerInfo(player)

players_new = {"players":players_ , "players_inactive":players_inactive_}

steamChamber.savePlayers(players_new)
