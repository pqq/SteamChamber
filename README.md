# StɛamChamber
> life is a game. play it!

## who & when
|who           |started      |
|:-------------|:------------|
|frode klevstul|november 2014|

## what it is
StɛamChamberis a "counter-strike: global offensive" competitive match statistics tracker.

## longer description
saves csgo player data as csv files (csv database). generates json results from the csv files.

## where
the solution is found on [steamchamber.com](http://steamchamber.com).

## steam's api
[documentation](https://docs.google.com/a/vxg.com/document/d/114tXOpqQRrGx0nKVVrRq_M_YvlBMFuDKW5xcKZniIiI/edit)

## server setup
[documentation](https://docs.google.com/a/vxg.com/document/d/1lXaTTyY8p3sLi2uEIqao9TrhdsId6VSj6IE5mX_eN9Y/edit)