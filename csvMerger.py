
# --------------------------------------------------------------------------
# file          : csvMerger.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : march 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import csv


def merge(filename_one, filename_two, filename_out):
    """
    important:
    file 2 needs to contain ALL columns found in file 1,
    but file 1 can have less columns than file 2. if there
    are columns in file 1 that do not exist in file 2, then those
    columns will be dropped.
    """

    f1 = open(filename_one, "rb")
    f2 = open(filename_two, "rb")

    reader1         = csv.reader(f1, delimiter=',', quotechar='"')
    reader2         = csv.reader(f2, delimiter=',', quotechar='"')
    header1         = reader1.next()
    header2         = reader2.next()
    header_mapping  = {}
    lines           = {}

    # get column numbers for each heading in the new file
    i = 0
    for column2 in header2:
        header_mapping[column2] = i
        i += 1

    rowNo = 0

    # go through file one
    for row in reader1:
        line = {}

        # initialise dict, for avoiding key errors as not all columns exists in file1
        for i in xrange( len(header_mapping) ):
            line[i] = None

        # build line
        i = 0
        for c in row:
            try:
                line[ header_mapping[ header1[i] ] ] = c
                i += 1
            # key error will occur if column from file 1 do not exist in file 2
            except KeyError:
                # skip line, and drop data in column from file 1 that do not exist is file 2
                i += 1

        # build lines (new file)
        lines[rowNo] = line

        rowNo += 1

    # go through file two
    for row in reader2:
        line = {}
        i = 0
        for c in row:
            line[i] = c
            i += 1

        lines[rowNo] = line

        rowNo += 1

    f1.close()
    f2.close()

    # write result to new file
    outfile = open(filename_out, "w")

    headerOut = ""
    for h in header2:
        headerOut += h + ","
    outfile.write(headerOut[:-1] + "\n")

    for line in lines:
        lineOut = ""
        for i in range( len(header_mapping) ):
            if lines[line][i] == None:
                lineOut += ","
            else:
                lineOut += lines[line][i] + ","
        outfile.write(lineOut[:-1] + "\n")

    outfile.close()
