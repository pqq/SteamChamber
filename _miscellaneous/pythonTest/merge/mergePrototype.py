import csv

f1 = open('one.csv', 'rb')
f2 = open('two.csv', 'rb')

reader1 = csv.reader(f1, delimiter=',', quotechar='"')
reader2 = csv.reader(f2, delimiter=',', quotechar='"')
header1 = reader1.next()
header2 = reader2.next()
header_mapping = {}
lines = {}

# get column numbers for each heading in the new file
i = 0
for column2 in header2:
    header_mapping[column2] = i
    i += 1

rowNo = 0

# go through file one
for row in reader1:
    line = {}

    # initialise dict, for avoiding key errors as not all columns exists in file1
    for i in xrange( len(header_mapping) ):
        line[i] = None

    # build line
    i = 0
    for c in row:
        line[ header_mapping[ header1[i] ] ] = c
        i += 1

    # build lines (new file)
    lines[rowNo] = line

    rowNo += 1

# go through file two
for row in reader2:
    #print row

    line = {}
    i = 0
    for c in row:
        line[i] = c
        i += 1

    lines[rowNo] = line

f1.close()
f2.close()


for h in header2:
    print h,

print ""

for line in lines:

    for i in range( len(header_mapping) ):
        print lines[line][i],
    print ""
