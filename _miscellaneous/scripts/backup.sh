#!/bin/bash
#Purpose = Backup of Important Data
#Created on 2015-03-20
#Author = Frode Klevstul
#Version 1.0
#START

# ref : http://www.broexperts.com/2012/06/how-to-backup-files-and-directories-in-linux-using-tar-cron-jobs/

TIME=`date +'%Y%m%d_%H%M'`                      # timestamp
FILENAME=backup-$TIME.tar.gz                    # backup name format
SRCDIR=/var/www/www.steamchamber.com/database   # source of backup
DESDIR=/home/vapor/backup                       # destination of backup
tar -cpzf $DESDIR/$FILENAME -P $SRCDIR          # run backup

#END 
