
# --------------------------------------------------------------------------
# file          : rss.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : april 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import cgitb
cgitb.enable()                                                                                                                              # write error messages in a web supported format
import datetime
import os
import urllib
import ast
import steamChamber


def outputFeedEntriesAsRss(steamid):
    """
    connects to the foreneNo database and outputs the feed entries as RSS
    """

    rss_link        = steamChamber.url_base_path + "app/#/players/"
    return_object   = urllib.urlopen(steamChamber.url_base_path + "rpc.py?method=GetStatForPlayerAsJson&steamid="+steamid)

    # convert string to dictionary
    dictionary = ast.literal_eval(return_object.read())

    print 'Content-Type: application/rss+xml; charset="UTF-8"'
    print ''
    print '<?xml version="1.0" encoding="UTF-8" ?>'
    print '<rss version="2.0">'
    print '<channel>'
    print '<title>St&#603;amChamber match stat for ' +dictionary["player"]["personaname"]+ '</title>'
    print '<link>http://www.steamchamber.com</link>'
    print '<description>feed presented by St&#603;amChamber</description>'
    print '<lastBuildDate>' + str(datetime.datetime.now()) + '</lastBuildDate>'
    print '<generator>St&#603;amChamber RSS generator</generator>'
    print '<docs>http://blogs.law.harvard.edu/tech/rss</docs>'

    for element in reversed(dictionary["statistics"]):
        print '<item>'
        print '<title>' + element["fetched_timestamp"] + '</title>'
        print '<link>' + rss_link + dictionary["player"]["steamid"] + '</link>'
        print '<description>'
        print ' // '
        print '    win status  : ' + str(element["last_match_win_status"]) + ' // '
        print '    won rounds  : ' + str(element["last_match_wins"]) + ' // '
        print '    lost rounds : ' + str(element["last_match_losses"]) + ' // '
        print '    kd          : ' + str(element["last_match_kd"]) + ' // '
        print '    kills       : ' + str(element["last_match_kills"]) + ' // '
        print '    deaths      : ' + str(element["last_match_deaths"]) + ' // '
        print '    mvps        : ' + str(element["last_match_mvps"]) + ' // '
        print '    score       : ' + str(element["last_match_contribution_score"]) + ' // '
        print '    accuracy    : ' + str(element["last_match_favweapon_accur"]) + ' // '
        print '</description>'
        print '<author>St&#603;amChamber</author>'

        if element["fetched_timestamp"]:
            try:
                if len(element["fetched_timestamp"]) == 10:
                    date_object = datetime.datetime.strptime(element["fetched_timestamp"], "%Y-%m-%d")
                elif len(element["fetched_timestamp"]) == 16:
                    date_object = datetime.datetime.strptime(element["fetched_timestamp"], "%Y-%m-%d %H:%M")
                else:
                    date_object = datetime.datetime.strptime("1976-08-18 02:20", "%Y-%m-%d %H:%M")
            except:
                date_object = datetime.datetime.strptime("1900-01-01 00:00", "%Y-%m-%d %H:%M")
        else:
            date_object = datetime.datetime.strptime("2015-01-01 00:00", "%Y-%m-%d %H:%M")

        print '<pubDate>' + date_object.strftime("%a, %d %b %Y %H:%M:%S") + '</pubDate>'
        print '</item>'

    print '</channel>'
    print '</rss>'


def main():
    """
    the subrutine that starts it all
    get the steamid from the url
    then we output the data as rss
    """

    steamid = os.environ['QUERY_STRING']
    outputFeedEntriesAsRss(steamid)


if "__main__" == __name__:
    main()
