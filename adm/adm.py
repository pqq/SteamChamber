
# --------------------------------------------------------------------------
# file          : adm.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : april 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  os
import  sys
import  json
import  urllib
from    xml.dom import minidom

# set full path to the directory of the steamChamber library
sys.path.insert(0, 'C:\Users\klevstul\_miscellaneous\Dropbox\Miscellaneous\projects\_gitHub\SteamChamber') # dev
# sys.path.insert(0, '/var/www/www.steamchamber.com') # prod

import  steamChamber


print "Content-Type: text/plain"
print
print "SteamChamber adm v0.1.150714"
print

qs              = os.environ['QUERY_STRING']
#qs              = "update-full-76561198053948648-mimetisk@gmail.com" # nifty for debugging purposes (running on the command line)
#qs              = "update-newcode-76561198084759630-mimetisk@gmail.com"
#qs              = "add-full-76561198084759630-mimetisk@gmail.com"
#qs              = "update-unpause-76561198084759630-mimetisk@gmail.com"
qsa             = qs.split('-')

if (qsa[0] == "add" or qsa[0] == "update") and (qsa[1] == "free" or qsa[1] == "full" or qsa[1] == "sponsored" or qsa[1] == "pause" or qsa[1] == "unpause" or  qsa[1] == "newcode"):
    print qsa[0] + " user '" + qsa[2] + "', subscription/action given is '" + qsa[1] + "'"

    player  = steamChamber.requestPlayerProfileInfo(qsa[2])
    steamid = player["steamid"]

    # did not find the steam id for this profile
    if steamid == 0:
        print "no valid steam id found"

    # steam id found
    else:
        print "found steam id " + steamid

        # get all steamchamber subscribers
        players = steamChamber.getPlayers()

        # create new player object
        new_player                  = {}
        new_player["steamid"]       = str(steamid)
        new_player["personaname"]   = player["personaname"]
        new_player["realname"]      = player["realname"]
        new_player["avatar"]        = player["avatar"]
        new_player["subscription"]  = qsa[1]
        if len(qsa) == 4:
            new_player["email"]      = qsa[3]
        else:
            new_player["email"]      = ""

        # check if player profile is public
        if player["privacyState"] != "public":
            print "privacy state is '" + player["privacyState"] + "' (needs to be public), player not added!"

            # inform user
            steamChamber.sendMessage(new_player, "email", "Registration error: Private profile", "You tried subscribing using a private Steam profile. Sorry, but that is not possible. Please change your privacy settings for your Steam to be public. Wait for a few minutes, and try to sign up again.")

            # exit program
            sys.exit()

        # generate new request key and url
        req_key = steamChamber.getUniqueRequestKey()
        req_url = steamChamber.url_base_path + "ua/index.py?" + steamid + "-" + req_key
        # generate profile url
        pro_url = steamChamber.url_base_path + "app/#/players/" + steamid

        # players consist of active and inactive players
        players_                = players["players"]
        players_inactive_       = players["players_inactive"]

        # if we are generating a new request code
        if qsa[0] == "update" and qsa[1] == "newcode":

            if steamChamber.isSubscriber(steamid):
                # delete existing request key (if any)
                steamChamber.deleteRequestKey(steamid)

                # save new request key
                steamChamber.saveRequestKey(steamid, req_key)

                # inform user
                steamChamber.sendMessage(new_player, "email", "New ua URL", "We have generated a new user admin (ua) URL for you. Your new URL is: " + req_url)

                # print info
                print "ua url:  " + req_url
                print "new request code generated!"

            else:
                print "can not generate new request codes for none-subscribers!"

            # exit program
            sys.exit()

        # if we are altering an existing subscriber
        if steamChamber.isSubscriber(steamid):

            # update existing player
            if qsa[0] == "update":

                # used to get registerred email, and previous subscription type
                subscriber = steamChamber.getPlayer(steamid)

                # preserve old email address, if no new one is given
                if len(new_player["email"]) == 0 and "email" in subscriber:
                    new_player["email"] = subscriber["email"]

                # pausing / unpausing a subscription
                if qsa[1] == "pause" or qsa[1] == "unpause":

                    if qsa[1] == "pause" and "pause" not in subscriber["subscription"]:
                        new_player["subscription"] = qsa[1] + "-" + subscriber["subscription"]

                        # inform user
                        steamChamber.sendMessage(new_player, "email", "Subscription has been paused", "Your subscription has been paused. For more information, please visit your private user admin page on SteamChamber.com.")

                        # print info
                        print "subscription paused!"

                    # subscription is already paused, so keep the state
                    elif qsa[1] == "pause":
                        new_player["subscription"] = subscriber["subscription"]

                        print "subscription already paused. nothing changed."
                        sys.exit()

                    # unpausing / reactivating the subscription
                    elif qsa[1] == "unpause" and "pause" in subscriber["subscription"]:
                        new_player["subscription"] = subscriber["subscription"].split('-')[1]

                        # inform user
                        steamChamber.sendMessage(new_player, "email", "Subscription has been activated", "Your subscription has been activated. For more information, please visit your private user admin page on SteamChamber.com.")

                        # print info
                        print "subscription activated (unpaused)!"

                    # unpausing a subscription that is not paused
                    elif qsa[1] == "unpause":

                        print "subscription already active. nothing changed."
                        sys.exit()

                # updating subscription type, email and request key
                else:

                    # delete existing request key (if any)
                    steamChamber.deleteRequestKey(steamid)

                    # save new request key
                    steamChamber.saveRequestKey(steamid, req_key)

                    # inform user
                    steamChamber.sendMessage(new_player, "email", "Subscription updated", "Your subscription had been updated. For more info, please visit " + req_url)

                    # print info
                    print "ua url:  " + req_url

                # remove existing player from player array
                players_ = steamChamber.removePlayerFromArray(players_, steamid)

                # if player is in list of inactive players, we remove player from this list as well
                players_inactive_ = steamChamber.removePlayerFromArray(players_inactive_, steamid)

                # re-add player to array, with new subscription assigned
                players_.append(new_player);

                # make dict of players
                players_new = {"players":players_ , "players_inactive":players_inactive_}

                # backup players file
                steamChamber.backupPlayers()

                # save players file
                steamChamber.savePlayers(players_new)

                print "player updated!"

            # player exists, not an update, we exit()
            else:
                print "player already exists!"
                sys.exit()

        # adding new player
        else:
            print "adding new player!"

            # backup of players.json
            steamChamber.backupPlayers()

            # add new player to list of active players
            players_.append(new_player)

            # make dict of players
            players_new = {"players":players_ , "players_inactive":players_inactive_}

            # write new updated player file
            steamChamber.savePlayers(players_new)

            # save new request key
            steamChamber.saveRequestKey(steamid, req_key)

            # inform user
            steamChamber.sendMessage(new_player, "email", "Welcome to SteamChamber", "We're glad to have you onboard!\nYour profile can be viewed at " + pro_url + "\nFor more info about your subscription, please visit " + req_url)

            # print info
            print "pro url: " + pro_url
            print "ua url:  " + req_url
            print "new player added!"
