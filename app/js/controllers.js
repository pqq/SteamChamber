/* verified with jshint.com */
/* global angular */


var playerControllers = angular.module("playerControllers", []);


playerControllers.controller("PlayerListCtrl", ["$scope", "$http", "$location",
    function ($scope, $http, $location) {
        "use strict";

        document.title = 'St\u03B5amChamber';
        document.body.style.backgroundImage     = "url('/app/images/backgrounds/oneBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        $http.get("/rpc.py?method=GetPlayers").success(function (data) {
            $scope.players = data.response.players;
            $http.get("/rpc.py?method=GetCSNews").success(function (data) {
                $scope.news = data.response.appnews.newsitems;
            });
        });

        $scope.orderProp = "personaname";

        $scope.showPlayer = function (player) {
            $location.path("/players/" + player.steamid);
        };

    }]);


playerControllers.controller("PlayerDetailsCtrl", ["$scope", "$routeParams", "$http", "ngDialog",
    function ($scope, $routeParams, $http, ngDialog) {
        "use strict";

        $http.get("/rpc.py?method=GetPlayerSummaries&steamid=" + $routeParams.steamid).success(function (data) {
            $scope.playerProfileInfo = data.response.players[0];
            if ($scope.playerProfileInfo) {
                document.title = $scope.playerProfileInfo.personaname + ' @ St\u03B5amChamber';
            } else {
                document.title = 'St\u03B5amChamber';
            }
        });

        document.body.style.backgroundImage     = "url('/app/images/backgrounds/twoBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        $http.get("/rpc.py?method=GetPlayerSteamchamberStatus&steamid=" + $routeParams.steamid).success(function (data) {
            $scope.playerSteamchamberStatus = data.response;

            if ($scope.playerSteamchamberStatus.is_subscriber === true) {
                $http.get("/rpc.py?method=GetExtrasForPlayer&steamid=" + $routeParams.steamid).success(function (data) {
                    if (data.response) {
                        $scope.playerExtras = data.response.statistics;
                    }
                });

                $http.get("/rpc.py?method=GetFriendList&steamid=" + $routeParams.steamid).success(function (data) {
                    if (data.response) {
                        $scope.friends = data.response.friendslist.friends;
                    }
                });

                $http.get("/rpc.py?method=GetStatForPlayerAsJson&steamid=" + $routeParams.steamid).success(function (data) {
                    $scope.player = data.response;
                    $scope.steamid = $routeParams.steamid;
                    $http.get("/rpc.py?method=GetItemsSchema").success(function (data) {
                        $scope.items = data.response.result.items;
                    });
                    if ($scope.playerProfileInfo) {
                        $http.get("/rpc.py?method=GetQuote").success(function (data) {
                            $scope.quote = data.response.quote.replace(/#name/g, $scope.playerProfileInfo.personaname);
                        });
                    }

                    // ------------------------------------------
                    // example chart
                    // ------------------------------------------
                    //   var chart1 = {};
                    //    chart1.type = "LineChart";
                    //    chart1.cssStyle = "height:400px; width:600px;";
                    //    chart1.data = {"cols": [
                    //        {id: "date-id", label: "date", type: "string"},
                    //        {id: "mvp-id", label: "mvps", type: "number"},
                    //        {id: "score-id", label: "score/10", type: "number"},
                    //        {id: "result", label: "result", type: "number"}
                    //    ], "rows": [
                    //        {c: [
                    //            {v: "2015-03-09 12:07"},
                    //            {v: 3, f: "42 items"},
                    //            {v: 4, f: "40 s"},
                    //            {v: 4}
                    //        ]},
                    //        {c: [
                    //            {v: "2015-03-09 15:07"},
                    //            {v: 0},
                    //            {v: 2.5, f: "25 stock"},
                    //            {v: -2}
                    //        ]},
                    //        {c: [
                    //            {v: "2015-03-09 19:32"},
                    //            {v: 8},
                    //            {v: 8, f:"81 s"},
                    //            {v: 6}
                    //
                    //        ]}
                    //    ]};
                    //
                    //    chart1.options = {
                    //        "title": "Match data",
                    //        "isStacked": "false",
                    //        "fill": 20,
                    //        "displayExactValues": "true",
                    //        "vAxis": {
                    //            "title": "Result", "gridlines": {"count": 5}
                    //        },
                    //        "hAxis": {
                    //            "title": "Date"
                    //        }
                    //    };
                    //
                    //    chart1.formatters = {};
                    //
                    //    $scope.chart = chart1;
                    // ------------------------------------------
                    var indexes = [], stats = [], cols = [], rows = [], i, current_stat, c, chart1 = {};
                    if ($scope.player.statistics !== undefined) {
                        if ($scope.player.statistics[0]) {
                            indexes = Object.keys($scope.player.statistics[0]);
                        }
                        stats       = $scope.player.statistics;
                    }

                    // build columns
                    for (i = 0; i < indexes.length; i = i + 1) {
                        if (indexes[i] === "fetched_timestamp") {
                            cols[0] = {id: "fetched_timestamp-id", label: "date", type: "string"};
                        } else if (indexes[i] === "last_match_win_status") {
                            cols[1] = {id: "last_match_win_status-id", label: "result", type: "number"};
                        } else if (indexes[i] === "last_match_mvps") {
                            cols[2] = {id: "last_match_mvps-id", label: "mvps", type: "number"};
                        } else if (indexes[i] === "last_match_contribution_score") {
                            cols[3] = {id: "last_match_contribution_score-id", label: "score/10", type: "number"};
                        } else if (indexes[i] === "last_match_kd") {
                            cols[4] = {id: "last_match_kd-id", label: "kills per death", type: "number"};
                        }
                    }

                    // build rows
                    for (i = 0; i < stats.length; i = i + 1) {
                        current_stat = stats[i];
                        c = [];

                        c[0] = {v: current_stat.fetched_timestamp };
                        c[1] = {v: current_stat.last_match_win_status };
                        c[2] = {v: current_stat.last_match_mvps };
                        c[3] = {v: current_stat.last_match_contribution_score / 10 };
                        c[4] = {v: Math.round(current_stat.last_match_kd * 10) / 10 };

                        if (current_stat.last_match_type === "competitive") {
                            rows.push({c: c});
                        }
                    }

                    chart1.type     = "AreaChart";
                    chart1.cssStyle = "height:500px; width:100%;";
                    chart1.data     = {
                        "cols": cols,
                        "rows": rows
                    };

                    chart1.options = {
                        "title": "",
                        "titleTextStyle": {color: "#aaaaaa"},
                        "isStacked": "false",
                        "fill": 20,
                        "displayExactValues": "true",
                        "vAxis": {
                            "gridlines": {"count": 5}
                        },
                        "hAxis": {
                            "textPosition": "none"
                        },
                        "backgroundColor": "#121212",
                        "chartArea": {"width": "95%", "height": "80%"},
                        "legend": {"position": "bottom"},
                        "legendTextStyle": {color: "#666666"},
                        "colors": ["red", "yellow", "blue", "green"]
                    };

                    chart1.formatters = {};

                    $scope.chart = chart1;

                    // show match details
                    $scope.showMatchDetails = function (stat) {
                        ngDialog.open({ template: 'modalTemplates/matchDetails.html', data: {stat: stat} });
                    };

                    // show friends comparison
                    $scope.showFriendsComparison = function (steamid, friends) {
                        ngDialog.open({
                            template: 'modalTemplates/friendsList.html',
                            data: {friends: friends, steamid: steamid},
                            controller: 'FriendsListPopupCtrl'
                        });
                    };

                });
            }
        });

    }]);


playerControllers.controller("WallOfAwesomenessCtrl", ["$scope", "$http", "$location", "$document",
    function ($scope, $http, $location, $document) {
        "use strict";

        document.title = 'St\u03B5amChamber\'s wall of awesomeness';
        document.body.style.backgroundImage     = "url('/app/images/backgrounds/twoBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        $http.get("/rpc.py?method=GetWallOfAwesomeness").success(function (data) {
            $scope.wallOfAwesomeness = data.response;
        });

        $scope.getLocation = function (doEncode) {
            var location = $location.absUrl();
            if (doEncode) {
                location = encodeURIComponent(location);
            }
            return location;
        };

        $scope.getTitle = function (doEncode) {
            var title = $document[0].title;
            if (doEncode) {
                title = encodeURIComponent(title);
            }
            return title;
        };

    }]);


playerControllers.controller("PlayerCompareCtrl", ["$scope", "$http", "$routeParams",
    function ($scope, $http, $routeParams) {
        "use strict";

        document.title = 'St\u03B5amChamber comparison';
        document.body.style.backgroundImage     = "url('/app/images/backgrounds/twoBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        var players, i, steamid;

        players             = $routeParams.steamidList.split(",");
        $scope.players      = players;
        $scope.playerData   = {};

        $scope.getHighscore = function (theObject, parameterName, maxOrMin) {
            var highscore, thisScore = 0, property;

            if (maxOrMin === "min") {
                highscore = 9999999;
            } else {
                highscore = -9999999;
            }

            if (theObject !== undefined) {

                for (property in theObject) {
                    if (theObject.hasOwnProperty(property)) {
                        if (theObject[property] !== undefined) {
                            thisScore = theObject[property][parameterName];
                        }
                        if (maxOrMin === "min") {
                            if (thisScore < highscore) {
                                highscore = thisScore;
                            }
                        } else {
                            if (thisScore > highscore) {
                                highscore = thisScore;
                            }
                        }
                    }
                }
            }

            return highscore;
        };

        function getPlayerData_playerSummaries(steamid) {
            $scope.playerData.playerSummaries = {};
            $http.get("/rpc.py?method=GetPlayerSummaries&steamid=" + steamid).success(function (data) {
                $scope.playerData.playerSummaries[steamid] = data.response.players[0];
            });
        }

        function getPlayerData_playerExtras(steamid) {
            $scope.playerData.playerExtras = {};
            $http.get("/rpc.py?method=GetExtrasForPlayer&steamid=" + steamid).success(function (data) {
                $scope.playerData.playerExtras[steamid] = data.statistics;
            });
        }

        function getPlayerData(steamid) {
            // get subscriber status
            $scope.playerData.is_subscriber = {};
            $http.get("/rpc.py?method=GetPlayerSteamchamberStatus&steamid=" + steamid).success(function (data) {
                $scope.playerData.is_subscriber[steamid] = data.is_subscriber;

                if (data.is_subscriber === true) {
                    // get player summaries
                    getPlayerData_playerSummaries(steamid);

                    // get player's extras (extra data)
                    getPlayerData_playerExtras(steamid);
                }
            });
        }

        // we compare no more than 5 players at a time
        if (players.length > 5) {
            $scope.tooManyPlayers = true;
        } else {
            for (i = 0; i < players.length; i = i + 1) {
                steamid = players[i];
                getPlayerData(steamid);
            }
        }

    }]);


var headerAndFooterControllers = angular.module("headerAndFooterControllers", []);


headerAndFooterControllers.controller("HeaderAndFooterCtrl", ["$scope", "ngDialog",
    function ($scope, ngDialog) {
        "use strict";
        $scope.showAbout = function () {
            ngDialog.open({template: 'modalTemplates/about.html'});
        };

        $scope.showContact = function () {
            ngDialog.open({template: 'modalTemplates/contact.html'});
        };
    }]);


var welcomeControllers = angular.module("welcomeControllers", []);


welcomeControllers.controller("WelcomeCtrl", ["$scope", "$location", "ngDialog",
    function ($scope, $location, ngDialog) {
        "use strict";

        document.title = 'St\u03B5amChamber';
        document.body.style.backgroundImage     = "url('/app/images/backgrounds/oneBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        $scope.goToSignupPage = function () {
            var newUrl = "/signup";
            $location.path(newUrl);
        };

        $scope.goToPlayerPage = function (steamid) {
            var newUrl = "/players/" + steamid;
            $location.path(newUrl);
        };

    }]);


var signupControllers = angular.module("signupControllers", []);


signupControllers.controller("SignupCtrl", ["$scope", "$location", "ngDialog",
    function ($scope, $location, ngDialog) {
        "use strict";

        document.title = 'St\u03B5amChamber';
        document.body.style.backgroundImage     = "url('/app/images/backgrounds/oneBg.jpg')";
        document.body.style.backgroundRepeat    = "repeat-x";
        document.body.style.backgroundPosition  = "0px 10px";
        document.body.style.backgroundSize      = "100%";

        $scope.showContact = function () {
            ngDialog.open({template: 'modalTemplates/contact.html'});
        };

        $scope.showSignupFull = function () {
            ngDialog.open({
                template: 'modalTemplates/signupFull.html',
                controller: 'SignupPopupCtrl'
            });
        };

        $scope.showSignupFree = function () {
            ngDialog.open({
                template: 'modalTemplates/signupFree.html',
                controller: 'SignupPopupCtrl'
            });
        };

    }]);


var popupControllers = angular.module("popupControllers", []);


popupControllers.controller("FriendsListPopupCtrl", ["$scope", "$location",
    function ($scope, $location) {
        "use strict";

        $scope.objectSize = function (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key) && obj[key]) {
                    size = size + 1;
                }
            }
            return size;
        };

        $scope.numberOfFriends = function (friends, onlySubscribingFriends) {
            var size = 0, i;

            for (i = 0; i < friends.length; i = i + 1) {

                if (onlySubscribingFriends) {
                    if (friends[i].is_subscriber === true && friends[i].subscription === 'full') {
                        size = size + 1;
                    }
                } else {
                    size = size + 1;
                }
            }
            return size;
        };

        $scope.compareListModel = {};
        $scope.doCompare = function (steamid) {
            var theObject, property, comparePlayersString = "", compareUrl;
            theObject = $scope.compareListModel;

            if (theObject !== undefined) {
                for (property in theObject) {
                    if (theObject.hasOwnProperty(property)) {
                        if (theObject[property]) {
                            comparePlayersString = comparePlayersString + property + ",";
                        }
                    }
                }
            }

            comparePlayersString = comparePlayersString.substring(0, comparePlayersString.length - 1);
            compareUrl = "/compare/" + steamid + "," + comparePlayersString;
            $location.path(compareUrl);
            $scope.closeThisDialog();
        };
    }]);


popupControllers.controller("SignupPopupCtrl", ["$scope", "$location",
    function ($scope, $location) {
        "use strict";

        window.submitDone = false;

        window.jumpToPage = function (theUrl) {
            window.location = theUrl;
            $scope.closeThisDialog();
        };

    }]);
