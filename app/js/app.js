/*global $, angular*/

var SteamChamberApp = angular.module('SteamChamberApp', [
    'ngRoute',
    'ngSanitize',
    'playerControllers',
    'headerAndFooterControllers',
    'welcomeControllers',
    'signupControllers',
    'popupControllers',
    'steamChamberFilters',
    'googlechart',
    'ngDialog',
    'twitter.timeline'
]);

SteamChamberApp.config(['$routeProvider', function ($routeProvider) {
    "use strict";
    $routeProvider.
        when('/players', {
            templateUrl: 'partials/playersList.html',
            controller: 'PlayerListCtrl'
        }).
        when('/players/:steamid', {
            templateUrl: 'partials/playerDetails.html',
            controller: 'PlayerDetailsCtrl'
        }).
        when('/compare/:steamidList', {
            templateUrl: 'partials/playerCompare.html',
            controller: 'PlayerCompareCtrl'
        }).
        when('/awesomeness', {
            templateUrl: 'partials/wallOfAwesomeness.html',
            controller: 'WallOfAwesomenessCtrl'
        }).
        when('/welcome', {
            templateUrl: 'partials/welcome.html',
            controller: 'WelcomeCtrl'
        }).
        when('/signup', {
            templateUrl: 'partials/signup.html',
            controller: 'SignupCtrl'
        }).
        when('/signupCompleteFree', {
            templateUrl: 'partials/signupCompleteFree.html',
            controller: 'SignupCtrl'
        }).
        when('/signupCompleteFull', {
            templateUrl: 'partials/signupCompleteFull.html',
            controller: 'SignupCtrl'
        }).
        otherwise({
            redirectTo: '/welcome'
        });
}]);

SteamChamberApp.config(['ngDialogProvider', function (ngDialogProvider) {
    "use strict";
    ngDialogProvider.setDefaults({
        className:            'ngdialog-theme-plain',
        plain:                false,
        showClose:            true,
        closeByDocument:      true,
        closeByEscape:        true,
        appendTo:             false
    });
}]);
