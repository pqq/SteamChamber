/*global $, angular*/

/*
oveview:
http://en.wikipedia.org/wiki/Unicode_symbols

nifty unicode symbols
---------------------
knife       &#128298;
fire        &#128293;
pistol      &#128299;
bomb        &#128163;
bulls eye   &#127919;
mushroom    &#127812;
skull       &#128128;
star        &#9734;
thumbs up   &#128077;
thumbs down &#128078;
yinyang     &#9775;

*/


angular.module('steamChamberFilters', [])

    .filter('matchResultFilter', function () {
        "use strict";
        return function (input, matchType, winStatus) {
            var retVal;

            if (input !== undefined) {
                if (matchType === "competitive") {
                    if (Number(winStatus) > 0) {
                        retVal = "win";
                    } else if (Number(winStatus) === 0) {
                        retVal = "draw";
                    } else if (Number(winStatus) < 0) {
                        retVal = "loss";
                    } else {
                        retVal = "n/a";
                    }
                } else {
                    retVal = "";
                }
            }

            return retVal;
        };

    }).filter('matchResultToColourFilter', function () {
        "use strict";
        return function (input) {
            var retVal, inputs;
            inputs = input.split(":");

            if (input !== undefined) {
                if (inputs[0] === "competitive") {
                    if (Number(inputs[1]) > 0) {
                        retVal = "success";
                    } else if (Number(inputs[1]) === 0) {
                        retVal = "warning";
                    } else if (Number(inputs[1]) < 0) {
                        retVal = "danger";
                    } else {
                        retVal = "info";
                    }
                } else if (inputs[0] === "deathmatch") {
                    retVal = "active";
                } else {
                    retVal = "info";
                }
            }

            return retVal;
        };

    }).filter('matchTypeFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                if (input === "competitive") {
                    retVal = "co";
                } else if (input === "deathmatch") {
                    retVal = "dm";
                } else {
                    retVal = "?";
                }
            }

            return retVal;
        };

    }).filter('dateFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = input;
                retVal = retVal.replace(/-/g, ".");
                retVal = retVal.replace(" ", " @ ");
                retVal = retVal.substring(2);
            }

            return retVal;
        };

    }).filter('numToPercentageFilter', function () {
        "use strict";
        return function (input, decimals, postString, preString) {
            var retVal;

            if (input !== undefined) {
                retVal = input;
                retVal = retVal * 100;

                if (Number(decimals) === 1) {
                    retVal = Math.round(retVal * 10) / 10;
                } else if (Number(decimals) === 2) {
                    retVal = Math.round(retVal * 100) / 100;
                } else {
                    retVal = Math.round(retVal);
                }

                retVal = String(retVal) + " %";
                if (postString !== undefined) {
                    retVal = retVal + " " + postString;
                }
                if (preString !== undefined) {
                    retVal = preString + " " + retVal;
                }
            }

            return retVal;
        };

    }).filter('roundNumberFilter', function () {
        "use strict";
        return function (input, decimals) {
            var retVal;

            if (input !== undefined) {
                retVal = input;

                if (Number(decimals) === 1) {
                    retVal = Math.round(retVal * 10) / 10;
                } else if (Number(decimals) === 2) {
                    retVal = Math.round(retVal * 100) / 100;
                } else {
                    retVal = Math.round(retVal);
                }
            }

            return String(retVal);
        };

    }).filter('weaponFilter', function () {
        "use strict";
        return function (input, items) {
            var retVal, i;

            retVal = input.toString();

            if (input !== undefined) {
                if (items !== undefined) {
                    for (i = 0; i < items.length; i = i + 1) {
                        if (input === items[i].defindex) {
                            retVal = items[i].name;
                            break;
                        }
                    }
                }
            }

            retVal = retVal.replace(/weapon_/, "");

            return retVal;
        };

    }).filter('mvpFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = input.toString();
                retVal = "&#9734; " + retVal;
            }

            return retVal;
        };

    }).filter('flagFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = input.toString().toLowerCase();
                retVal = "<img src=\"images/flags/" + retVal + ".png\" alt=\"" + retVal + "\" />";
            }

            return retVal;
        };

    }).filter('headshotFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = input.toString();
                retVal = "&#127919; " + retVal;
            }

            return retVal;
        };

    }).filter('winFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = input.toString();
                retVal = "<i class=\"fa fa-trophy\"></i> " + retVal;
            }

            return retVal;
        };

    }).filter('timeFilter', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = Math.round(input / (60 * 60));
                retVal = retVal.toString();
                retVal = "&#128338; " + retVal;
            }

            return retVal;
        };

    }).filter('ifDefined', function () {
        "use strict";
        return function (input, postString, preString) {
            var retVal;

            if (input !== undefined) {
                retVal = String(input);

                if (postString !== undefined) {
                    retVal = retVal + " " + postString;
                }
                if (preString !== undefined) {
                    retVal = preString + " " + retVal;
                }
            }

            return retVal;
        };

    }).filter('awesomenessValue', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                retVal = Number(input);
                retVal = Math.abs(retVal);

                if (retVal < 1) {
                    retVal = retVal * 100;
                    retVal = Math.round(retVal * 100) / 100;
                    retVal = String(retVal) + " %";
                }
            }

            return retVal;
        };

    }).filter('extrasParameterTranslation', function () {
        "use strict";
        return function (input) {
            var retVal;

            if (input !== undefined) {
                if (input === "hours_played") {
                    retVal = "hours played";
                } else if (input === "average_kills") {
                    retVal = "average kills";
                } else if (input === "total_win_status_mvps") {
                    retVal = "best win status (with mvps)";
                } else if (input === "average_kd") {
                    retVal = "average kd";
                } else if (input === "longest_win_streak") {
                    retVal = "win streak";
                } else if (input === "total_matches") {
                    retVal = "total matches";
                } else if (input === "average_contribution_score") {
                    retVal = "average score";
                } else if (input === "total_wins") {
                    retVal = "total wins";
                } else if (input === "average_deaths") {
                    retVal = "average deaths (less is more)";
                } else if (input === "total_win_status") {
                    retVal = "best win status";
                } else if (input === "average_accuracy") {
                    retVal = "average accuracy";
                } else if (input === "current_win_streak") {
                    retVal = "current win streak";
                } else if (input === "steamchamber_ranking") {
                    retVal = "St\u03B5amChamber ranking";
                } else if (input === "average_wins") {
                    retVal = "average wins";
                } else if (input === "highscore_kd") {
                    retVal = "highscore kd";
                } else if (input === "highscore_mvps") {
                    retVal = "highscore mvps";
                } else if (input === "highscore_kills") {
                    retVal = "highscore kills";
                } else if (input === "average_mvps") {
                    retVal = "average mvps";
                } else if (input === "highscore_accuracy") {
                    retVal = "highscore accuracy";
                } else if (input === "highscore_cscore") {
                    retVal = "highscore";
                } else if (input === "total_mvps") {
                    retVal = "total mvps";
                } else {
                    retVal = input;
                }
            }

            return retVal;
        };

    }).filter('reverse', function () {
        "use strict";
        return function (items) {
            if (items) {
                return items.slice().reverse();
            }
        };
    });
