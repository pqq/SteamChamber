
# --------------------------------------------------------------------------
# file          : generateJson.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : november 2014
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  csv
import  codecs
import  difflib
import  steamChamber


line_number = 0
cell_number = 0
header      = []
ratio_limit = 1.0


# select the following data from the csv database files
selected_columns    = [
                          "fetched_timestamp"

                        , "total_kills_headshot"
                        , "total_matches_played"
                        , "total_matches_won"
                        , "total_mvps"
                        , "total_shots_fired"
                        , "total_shots_hit"
                        , "total_time_played"

                        , "last_match_contribution_score"
                        , "last_match_ct_wins"
                        , "last_match_damage"
                        , "last_match_deaths"
                        , "last_match_dominations"
                        , "last_match_favweapon_hits"
                        , "last_match_favweapon_id"
                        , "last_match_favweapon_kills"
                        , "last_match_favweapon_shots"
                        , "last_match_kills"
                        , "last_match_max_players"
                        , "last_match_money_spent"
                        , "last_match_mvps"
                        , "last_match_revenges"
                        , "last_match_rounds"
                        , "last_match_t_wins"
                        , "last_match_wins"
                    ]

# csv (database) included parameters
fetched_timestamp               = 0     # timestamp for when match data was fetched
last_match_t_wins               = 0     # the total number of rounds terrorist side won - both teams
last_match_ct_wins              = 0     # the total number of rounds counter-terrorist side won - both teams
last_match_wins                 = 0     # the number of rounds player won
last_match_kills                = 0     # number of kills - kills in warmup period is included in this number
last_match_deaths               = 0     # number of deaths - deaths in warmup period is included
last_match_favweapon_shots      = 0     # not sure if warmup data is included
last_match_favweapon_hits       = 0     # not sure if warmup data is included
last_match_damage               = 0     # damage taken or given? not sure...
last_match_money_spent          = 0     # assume this include money in warm up as well
last_match_rounds               = 0     # total number of rounds played last match - not including warmup
last_match_max_players          = 0     # maximum (allowed?) players last match
last_match_mvps                 = 0     # number of mvps achieved in last match
last_match_contribution_score   = 0     # the (contribution) score, achived, excluding warm up, i believe
total_matches_played            = 0     # the number of competitive and casual matches played
total_matches_won               = 0     # the number of competitive and casual matches won
total_shots_fired               = 0     # shots fired in comp and causual mode
total_shots_hit                 = 0     # shots hit in comp and causual mode
total_kills_headshot            = 0     # headshots in comp and causual mode

# calculated / deducted parameters
last_match_losses               = 0
last_match_kd                   = 0
last_match_favweapon_accur      = 0
last_match_damage_pr_round      = 0
last_match_spent_pr_round       = 0
last_match_win_status           = 0
last_match_type                 = ""
total_win_ratio                 = 0
total_hit_ratio                 = 0
total_headshot_kill_ratio       = 0

# parameters to hold values from previous match
previous_total_matches_won  = 0

# get all database csv files
db_files = steamChamber.getAllDbCsvFiles()

# load all players
player_data = steamChamber.getPlayers()

# go through all db files
for db_file in db_files:
    with open(steamChamber.os_db_relative_path + db_file, "rb") as csvfile:
        csvreader               = csv.reader(csvfile, delimiter=',', quotechar='"')
        steamid                 = db_file.split(".",1)[0]                                                                                   # file name equals steam id
        player_name             = ""
        first_result_written    = False
        previous_file_content   = ""

        # get player name
        for player in player_data["players"]:
            if player["steamid"] == steamid:
                player_name = player["personaname"]

        # write output file
        print "] writing " + steamid + ".json"
        with codecs.open(steamChamber.os_json_relative_path+steamid+".json", "w", "utf-8-sig") as outfile:

            # player info
            outfile.write( "{\n" )
            outfile.write( "  \"player\": {\n" )
            outfile.write( "        \"personaname\": \"" + player_name + "\"\n" )
            outfile.write( "      , \"steamid\" : \""+steamid+"\"\n" )
            outfile.write( "  },\n" )

            # statistics
            outfile.write( "  \"statistics\": [\n" )
            line_number = 0

            for row in csvreader:
                if not row:
                    continue

                print "]  process row #" + str(line_number)
                file_content            = ""
                last_match_type         = ""
                last_match_max_players  = ""
                last_match_rounds       = ""

                if line_number == 0:
                    header = row

                else:
                    cell_number = 0
                    for cell in header:

                        if cell in selected_columns:
                            if cell_number == 0:
                                file_content += "        "
                            else:
                                file_content += "      , "

                            try:
                                float( row[cell_number] )
                                file_content += "\"" + cell + "\" : " + row[cell_number] + "\n"
                            except:
                                file_content += "\"" + cell + "\" : \"" + row[cell_number] + "\"\n"

                            if not row[cell_number]:                                                                                        # set NaN (empty / null) cells to 0
                                row[cell_number] = 0

                            # to calculate some extra parameters we need to store several specific parameters
                            if cell == "fetched_timestamp":
                                fetched_timestamp = row[cell_number]
                            elif cell == "last_match_t_wins":
                                last_match_t_wins = row[cell_number]
                            elif cell == "last_match_ct_wins":
                                last_match_ct_wins = row[cell_number]
                            elif cell == "last_match_wins":
                                last_match_wins = row[cell_number]
                            elif cell == "last_match_kills":
                                last_match_kills = row[cell_number]
                            elif cell == "last_match_deaths":
                                last_match_deaths = row[cell_number]
                            elif cell == "last_match_favweapon_shots":
                                last_match_favweapon_shots = row[cell_number]
                            elif cell == "last_match_favweapon_hits":
                                last_match_favweapon_hits = row[cell_number]
                            elif cell == "last_match_damage":
                                last_match_damage = row[cell_number]
                            elif cell == "last_match_money_spent":
                                last_match_money_spent = row[cell_number]
                            elif cell == "last_match_rounds":
                                last_match_rounds = row[cell_number]
                            elif cell == "last_match_max_players":
                                last_match_max_players = row[cell_number]
                            elif cell == "last_match_mvps":
                                last_match_mvps = row[cell_number]
                            elif cell == "last_match_contribution_score":
                                last_match_contribution_score = row[cell_number]
                            elif cell == "total_matches_played":
                                total_matches_played = row[cell_number]
                            elif cell == "total_matches_won":
                                total_matches_won = row[cell_number]
                            elif cell == "total_shots_fired":
                                total_shots_fired = row[cell_number]
                            elif cell == "total_shots_hit":
                                total_shots_hit = row[cell_number]
                            elif cell == "total_kills_headshot":
                                total_kills_headshot = row[cell_number]

                            # avoid division by zero
                            if (int(last_match_favweapon_shots) == 0):
                                last_match_favweapon_shots = 1
                            if (int(total_matches_played) == 0):
                                total_matches_played = 1
                            if (int(total_shots_fired) == 0):
                                total_shots_fired = 1
                            if (int(last_match_deaths) == 0):
                                last_match_deaths = 1
                            try:
                                int(last_match_rounds)
                            except:
                                last_match_rounds = 1
                            if (int(last_match_rounds) == 0):
                                last_match_rounds = 1

                        cell_number += 1

                    # calculate our new parameters
                    last_match_losses           = int(last_match_rounds) - int(last_match_wins)
                    last_match_kd               = round( (float(last_match_kills) / float(last_match_deaths)), 4)
                    last_match_favweapon_accur  = round( (float(last_match_favweapon_hits) / float(last_match_favweapon_shots)), 4)
                    last_match_damage_pr_round  = int(last_match_damage) / int(last_match_rounds)
                    last_match_spent_pr_round   = int(last_match_money_spent) / int(last_match_rounds)
                    last_match_win_status       = -((int(last_match_rounds)/2.0) - int(last_match_wins))                                    # where positiv eq victory, 0 eq draw and negative eq loss. the higher the number, the bigger the win.
                    total_win_ratio             = round( float(total_matches_won) / float(total_matches_played), 4)
                    total_hit_ratio             = round( float(total_shots_hit) / float(total_shots_fired), 4)
                    total_headshot_kill_ratio   = round( float(total_kills_headshot) / float(total_shots_fired), 4)                         # shots that hits heads and kill (there are headshots that do not kill as well)

                    # determine match type
                    # (have not spent much time in identifying other matches than competitive, as those are the focus for SteamChamber)
                    if (
                        int(last_match_max_players)     == 10 and
                        int(last_match_rounds)          <= 30 and
                        int(last_match_rounds)           > 1 and
                        int(last_match_money_spent)      > 0 and
                        (
                            int(last_match_wins)         > 8 or
                            int(last_match_losses)       > 8
                        ) and
                        (
                            int(last_match_wins)        <= 16 and
                            int(last_match_losses)      <= 16
                        )
                    ):
                        last_match_type = "competitive"

                        # if we have a win...
                        if last_match_win_status > 0:
                            # ... we should have an increase in total matches, if we do not have this, and mvp count equals 0, it is most likely another match type (sc-110)
                            if int(last_match_mvps) == 0 and int(total_matches_won) == int(previous_total_matches_won):
                                last_match_type = "unknown"
                                print "]    zero mvps and no increase in total wins, not likely a competitive match (" + fetched_timestamp + ")"

                        # if we have a loss, or a draw, and mvps equals zero ...
                        elif last_match_win_status <= 0 and int(last_match_mvps) == 0:
                            # if the score is 45 or above, or the kills is 25 or above, it is very unlikely that player has not received any mvps, hence the match type is most likely not competitve (sc-179)
                            if int(last_match_contribution_score) >= 45 or int(last_match_kills) >= 25:
                                last_match_type = "unknown"
                                print "]    score 45 or more, kills 25 or more, and no mvps. not likely a competitive match (" + fetched_timestamp + ")"

                        # if number kills is 40 or above, it is very unlikely that number of kills is higher than the score. remember, except for during warmup one kill gives one point. if you get 30 or more kills, you are good, and most likely you would have gotten som mvps, and done some plants and/or defuses, which gives points (sc-194)
                        if int(last_match_kills) >= 40 and int(last_match_kills) > int(last_match_contribution_score):
                            last_match_type = "unknown"
                            print "]    kills 40 or more, and score lower than number of kills. not likely a competitive match (" + fetched_timestamp + ")"

                        if last_match_type == "competitive":
                            print "]    competitive match identified (" + fetched_timestamp + ")"

                    elif int(last_match_max_players) == 16 and int(last_match_rounds) == 1:
                        last_match_type = "deathmatch"
                        print "]    deathmatch match identified (" + fetched_timestamp + ")"
                    else:
                        last_match_type = "unknown"
                        print "]    unknown match type (" + fetched_timestamp + ")"

                    file_content += "      , \"last_match_losses\" : "            + str(last_match_losses)             + "\n"
                    file_content += "      , \"last_match_kd\" : "                + str(last_match_kd)                 + "\n"
                    file_content += "      , \"last_match_favweapon_accur\" : "   + str(last_match_favweapon_accur)    + "\n"
                    file_content += "      , \"last_match_damage_pr_round\" : "   + str(last_match_damage_pr_round)    + "\n"
                    file_content += "      , \"last_match_spent_pr_round\" : "    + str(last_match_spent_pr_round)     + "\n"
                    file_content += "      , \"last_match_win_status\" : "        + str(last_match_win_status)         + "\n"
                    file_content += "      , \"total_win_ratio\" : "              + str(total_win_ratio)               + "\n"
                    file_content += "      , \"total_hit_ratio\" : "              + str(total_hit_ratio)               + "\n"
                    file_content += "      , \"total_headshot_kill_ratio\" : "    + str(total_headshot_kill_ratio)     + "\n"
                    file_content += "      , \"last_match_type\" : \""            + str(last_match_type)               + "\"\n"

                    # the steam api delivers data that is very similar to each other (slightly updated data). hence we measure the similarty, for avoiding duplicates.
                    # we only do compare "last_match" paramters, as the other parameters might very well change despite not no new matches having been played.
                    these_lines             = file_content.split("\n")
                    these_lines_stripped    = ""
                    previous_lines          = previous_file_content.split("\n")
                    previous_lines_stripped = ""

                    for l in these_lines:
                        if "last_match" in l:
                            these_lines_stripped += l

                    for l in previous_lines:
                        if "last_match" in l:
                            previous_lines_stripped += l

                    seq = difflib.SequenceMatcher(a=str(these_lines_stripped).lower(), b=str(previous_lines_stripped).lower())

                    # nifty for debugging
                    #print "r=" + str(seq.ratio())

                    if last_match_type == "competitive" and seq.ratio() < ratio_limit:
                        if not first_result_written:
                            first_result_written = True
                        elif first_result_written:
                            outfile.write( "    ,\n" )

                        outfile.write( "    {\n" )
                        outfile.write(file_content)
                        outfile.write( "    }\n" )
                    elif seq.ratio() >= ratio_limit:
                        print "]    line #" + str(line_number) + " " + str(seq.ratio()) + " similarity with previous line. skip. (" + fetched_timestamp + ")"
                    else:
                        print "]    skip '" + last_match_type + "' match (" + fetched_timestamp + ")"

                    # store previous competitive data (for the next round in the loop)
                    previous_total_matches_won  = total_matches_won

                previous_file_content   = file_content
                line_number             += 1

            outfile.write( "  ]\n" )
            outfile.write( "}\n" )

        outfile.close()
