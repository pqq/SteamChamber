
# --------------------------------------------------------------------------
# file          : wallOfAwesomeness.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : april 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import os
import json
import codecs
import steamChamber


output_file_format  = "wallOfAwesomeness.json"

# get all extras files
files = steamChamber.getAllExtrasFiles()

all_extras          = {}
player_summaries    = {}
steamid             = 0
personaname         = ""
realname            = ""
highscores          = {}

# loop through all files, build one dict with all data
for filename in files:
    with codecs.open(os.path.join(steamChamber.os_json_relative_path,filename),"r",encoding="utf-8-sig") as f:
        all_extras[filename] = json.load(f)
    f.close()

# loop through all results
for element in all_extras:
    # get steam id
    steamid = all_extras[element]["player"]["steamid"]

    # a minimum of 10 wins is required for being considered for awesomeness
    if all_extras[element]["statistics"]["total_wins"] < 10:
        continue

    player = steamChamber.getPlayer(steamid)

    # in some cases there are extras files on the os for players that has been removed from players list, where player object will be empty, and we continue
    if not player:
        continue

    # generate highscores, as dict
    for elem in all_extras[element]["statistics"]:
        if not highscores.get(elem):
            highscores[elem]                = {}
            highscores[elem]["value"]       = -999

        if all_extras[element]["statistics"][elem] > highscores[elem]["value"]:
            highscores[elem]["value"]       = all_extras[element]["statistics"][elem]
            highscores[elem]["steamid"]     = steamid
            highscores[elem]["personaname"] = player["personaname"]
            highscores[elem]["realname"]    = player["realname"]

# nifty for debugging
#print highscores

# write data to os
local_file = open(os.path.join(steamChamber.os_json_relative_path,output_file_format), "w")
local_file.write( json.dumps(highscores, encoding = "utf-8", indent = 4) )
local_file.close()
