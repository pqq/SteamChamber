
# --------------------------------------------------------------------------
# file          : calculateExtras.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : april 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  os
import  json
import  codecs
import  steamChamber


output_file_format      = "[steamid]_extras.json"

# get all statistics files
statfiles = steamChamber.getAllJsonStatFiles()

# loop through all statistics files
for filename in statfiles:
    player_extras                       = {}
    steamid                             = os.path.splitext(filename)[0]
    player_extras["player"]             = {}
    player_extras["statistics"]         = {}
    player_extras["player"]["steamid"]  = steamid

    no_entries                          = 0

    highscore_mvps                      = 0
    highscore_cscore                    = 0
    highscore_kd                        = 0
    highscore_accuracy                  = 0
    highscore_kills                     = 0

    average_contribution_score          = 0
    average_kd                          = 0
    average_accuracy                    = 0
    average_kills                       = 0
    average_deaths                      = 0
    average_mvps                        = 0
    average_wins                        = 0

    longest_win_streak                  = 0
    total_win_status                    = 0
    total_win_status_mvps               = 0
    steamchamber_ranking                = 0
    current_win_streak                  = 0
    hours_played                        = 0
    total_mvps                          = 0
    total_wins                          = 0
    total_matches                       = 0

    # we do no calculate extras for simple subscription type
    if steamChamber.getSubscription(steamid) == "simple":
        steamChamber.debug("skipping player with id " + steamid + ", due to subscription type" )
        continue

    # open / load the json statistics file - for accessing the player's statistics
    # note: need to open as ut8-8-sig for avoiding "No JSON object could be decoded" error message
    with codecs.open(os.path.join(steamChamber.os_json_relative_path,filename),"r",encoding="utf-8-sig") as f:
        player_json_data = json.load(f)
    f.close()

    # loop through all stats
    for stat in player_json_data["statistics"]:
        no_entries += 1

        # summaries
        average_contribution_score  += stat["last_match_contribution_score"]
        total_win_status            += stat["last_match_win_status"]
        average_kd                  += stat["last_match_kd"]
        average_accuracy            += stat["last_match_favweapon_accur"]
        average_kills               += stat["last_match_kills"]
        average_deaths              += stat["last_match_deaths"]
        average_mvps                += stat["last_match_mvps"]
        total_mvps                  += stat["last_match_mvps"]
        if int(stat["last_match_win_status"] > 0): total_wins += 1

        # get newest values
        hours_played                = stat["total_time_played"]

        # -----
        # calculation of sc ranking points, plus "total_win_status_mvps", plus average_wins
        if stat["last_match_win_status"] > 0:
            average_wins += 1
            # only calculate positive win status points if player has at least 1 mvp in the match
            if stat["last_match_mvps"] > 0:
                total_win_status_mvps += stat["last_match_win_status"]
                steamchamber_ranking += stat["last_match_win_status"]
        elif stat["last_match_win_status"] < 0:
            steamchamber_ranking += stat["last_match_win_status"]
            total_win_status_mvps += stat["last_match_win_status"]

        # punish more deaths that kills
        if stat["last_match_kd"] < 1:
            steamchamber_ranking = steamchamber_ranking - 1 * stat["last_match_kd"]
        # reward double or more in kd
        elif stat["last_match_kd"] >= 2:
            steamchamber_ranking = steamchamber_ranking + 1 * stat["last_match_kd"]

        # mvps are positive
        steamchamber_ranking = steamchamber_ranking + stat["last_match_mvps"]

        #print steamchamber_ranking

        # / end of "scranking" calc
        # -----

        # win streak
        if stat["last_match_win_status"] > 0:
            current_win_streak += 1

            if current_win_streak > longest_win_streak:
                longest_win_streak = current_win_streak
        else:
            current_win_streak = 0

        # highscore mvps
        if stat["last_match_mvps"] > highscore_mvps:
            highscore_mvps = stat["last_match_mvps"]

        # highscore contribution score
        if stat["last_match_contribution_score"] > highscore_cscore:
            highscore_cscore = stat["last_match_contribution_score"]

        # highscore kd
        if stat["last_match_kd"] > highscore_kd:
            highscore_kd = stat["last_match_kd"]

        # highscore accuracy
        if stat["last_match_favweapon_accur"] > highscore_accuracy:
            highscore_accuracy = stat["last_match_favweapon_accur"]

        # highscore kills
        if stat["last_match_kills"] > highscore_kills:
            highscore_kills = stat["last_match_kills"]

    total_matches = no_entries

    if no_entries == 0:
        no_entries = 1

    # calculate averages
    average_contribution_score  = float(average_contribution_score) / no_entries
    average_kd                  = float(average_kd) / no_entries
    average_accuracy            = float(average_accuracy) / no_entries
    average_kills               = float(average_kills) / no_entries
    average_deaths              = float(average_deaths) / no_entries
    average_mvps                = float(average_mvps) / no_entries
    average_wins                = float(average_wins) / no_entries

    # avoid 0 for averages
    if average_deaths == 0: average_deaths = 999

    # add values to dictionary
    player_extras["statistics"]["average_contribution_score"]   = round(average_contribution_score, 0)
    player_extras["statistics"]["total_win_status"]             = total_win_status
    player_extras["statistics"]["total_win_status_mvps"]        = total_win_status_mvps
    player_extras["statistics"]["steamchamber_ranking"]         = round(steamchamber_ranking, 0)
    player_extras["statistics"]["current_win_streak"]           = current_win_streak
    player_extras["statistics"]["longest_win_streak"]           = longest_win_streak
    player_extras["statistics"]["highscore_mvps"]               = highscore_mvps
    player_extras["statistics"]["highscore_cscore"]             = highscore_cscore
    player_extras["statistics"]["highscore_kd"]                 = round(highscore_kd, 2)
    player_extras["statistics"]["average_kd"]                   = round(average_kd, 2)
    player_extras["statistics"]["highscore_accuracy"]           = highscore_accuracy
    player_extras["statistics"]["average_accuracy"]             = round(average_accuracy, 4)
    player_extras["statistics"]["highscore_kills"]              = highscore_kills
    player_extras["statistics"]["average_kills"]                = round(average_kills, 2)
    player_extras["statistics"]["average_deaths"]               = -round(average_deaths, 2)                                                 # do minus, to get a result same as everything else, the higher the number the better (reversing)
    player_extras["statistics"]["average_mvps"]                 = round(average_mvps, 2)
    player_extras["statistics"]["hours_played"]                 = hours_played/(60*60)
    player_extras["statistics"]["average_wins"]                 = round(average_wins, 4)
    player_extras["statistics"]["total_mvps"]                   = total_mvps
    player_extras["statistics"]["total_wins"]                   = total_wins
    player_extras["statistics"]["total_matches"]                = total_matches

    # nifty for debugging
    #print player_extras

    # write data to os
    local_file = open(os.path.join(steamChamber.os_json_relative_path,output_file_format.replace("[steamid]", steamid)), "w")
    local_file.write( json.dumps(player_extras, encoding = "utf-8", indent = 4) )
    local_file.close()
