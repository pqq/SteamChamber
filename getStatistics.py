
# --------------------------------------------------------------------------
# file          : getStatistics.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : november 2014
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  sys
import  time
import  getopt
import  steamChamber


def main(argv):
    """
    main
    """
    fetcher_begin   = 0
    fetcher_size    = 9999

    try:
        opts, args = getopt.getopt(argv,"hb:s:",["begin=","size="])
    except getopt.GetoptError:
      print "getStatistics.py -b <begin : integer> -s <size : integer>"
      sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print "getStatistics.py -b <begin : integer> -s <size : integer>"
            sys.exit()
        elif opt in ("-b", "--begin"):
            fetcher_begin = arg
        elif opt in ("-s", "--size"):
            fetcher_size = arg

    getStat(int(fetcher_begin), int(fetcher_size))


def getStat(fetcher_begin, fetcher_size):
    """
    get the statistics from the steam api
    """

    # get players
    players = steamChamber.getPlayers()["players"]

    player_number = 0

    # -----
    # loop through all steam ids
    # -----
    for player in players:
        player_number += 1

        if fetcher_size == 0:
            print "] fetch size reached, breaking the loop."
            break
        elif player["subscription"] != "full":
            if fetcher_begin >= player_number:
                print "] skip player #" + str(player_number) + " (subscription type is '" + player["subscription"] + "'), wait for player #" + str(fetcher_begin+1) + " instead of player #" + str(fetcher_begin)
            else:
                print "] skip player #" + str(player_number) + " (subscription type is '" + player["subscription"] + "')"
            fetcher_begin += 1                                                                                                              # need to add one to the begin position, as we should only count full subscription players in terms of where to begin fetching stats
            continue
        elif player_number < fetcher_begin:
            print "] skip player #" + str(player_number) + " (waiting for player #" + str(fetcher_begin) + ")."
            continue
        elif fetcher_size > 0:
            fetcher_size -= 1

        # reset steamid for each round in the loop
        steamid = player["steamid"]

        # get statistics for player
        stats = steamChamber.requestStatForPlayer(steamid)

        # store the statistics
        return_info = steamChamber.saveStatForPlayer(steamid, stats)

        print return_info


if __name__ == "__main__":
    """
    where it all starts
    """
    start_time = time.time()
    main(sys.argv[1:])
    print("--- %s seconds ---" % (time.time() - start_time))
