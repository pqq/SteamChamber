
# --------------------------------------------------------------------------
# file          : subscriptionChecker.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : may 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import json
import steamChamber


players             = steamChamber.getPlayers()
players_            = list(players["players"])                                                                                              # we need to make a copy of the list (http://henry.precheur.org/python/copy_list.html)
players_inactive_   = list(players["players_inactive"])

for player in players["players"]:
    print "] ******************** " + player["steamid"] + " ********************"
    steamid     = player["steamid"]
    player_data = steamChamber.requestPlayerProfileInfo(steamid)

    # test 1 : check if steam id is valid (profile info was found in above request)
    if not player_data:
        players_.remove(player)
        message = "unable to load steam profile."
        player["message"] = message
        player["subscription"] = "inactive-" + player["subscription"]
        players_inactive_.append(player)
        continue

    # test 2 : check if profile is public
    if not "public" in player_data["privacyState"]:
        players_.remove(player)
        message = "profile is not public."
        player["message"] = message
        player["subscription"] = "inactive-" + player["subscription"]
        players_inactive_.append(player)
        continue

    # test 3 : check if player is (in)active
    days_since_last_entry = steamChamber.getDaysSinceLastEntry(steamid)

    # move players to inactive, if no new entries for more than x days
    if days_since_last_entry > steamChamber.inactive_days_limit:
        players_.remove(player)
        message = "inactive player, " + str(days_since_last_entry) + " days since last database entry."
        player["message"] = message
        player["subscription"] = "inactive-" + player["subscription"]
        players_inactive_.append(player)
        continue

    # test 4 : if free subscription, check if player has sc advertisement in the player name, and has sc group set as primary group
    if player["subscription"] == "free":
        (subscription_valid, message) = steamChamber.isVerifiedSubscription(player_data)

        if not subscription_valid:
            players_.remove(player)
            player["message"] = message
            player["subscription"] = "inactive-" + player["subscription"]
            players_inactive_.append(player)

# build new players dictionary
players_new = {"players":players_ , "players_inactive":players_inactive_}

# nifty for debugging
#print json.dumps(
#      players_new
#    , encoding      = "utf-8"
#    , indent        = 4
#)

# save players dict to file
steamChamber.savePlayers(players_new)
