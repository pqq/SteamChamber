
# --------------------------------------------------------------------------
# file          : index.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : may 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------

import  os
import  sys
import  json
import  urllib

# set full path to the directory of the steamChamber library
sys.path.insert(0, 'C:\Users\klevstul\_miscellaneous\Dropbox\Miscellaneous\projects\_gitHub\SteamChamber') # dev
# sys.path.insert(0, '/var/www/www.steamchamber.com') # prod

import  steamChamber


print "Content-Type: text/html"
print
print """
<html>
<head>
    <meta charset="utf-8">
    <title>St&#603;amChamber ua</title>
    <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.2/cyborg/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="/app/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/app/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/app/images/favicon/favicon-16x16.png">

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-44703456-3', 'auto');
      ga('require', 'linkid', 'linkid.js');
      ga('send', 'pageview');
    </script>
</head>
<body style="padding: 10px;">
    <h1>St&#603;amChamber user administration</h1>
"""

qs     = os.environ['QUERY_STRING']
#qs      = "76561198084759630-f122ee40184011e59855448a5b25560a-fetchstat" # nifty for debugging purposes (when running on the command line)
#qs      = "76561198052092770-bfbee7405c7011e5abc0448a5b25560a-fetchstat"
qsa     = qs.split('-')
steamid = qsa[0]
key     = qsa[1]
if len(qsa) > 2:
    action = qsa[2]
else:
    action = ""

if steamChamber.isValidRequestKey(steamid, key) and len(qsa) == 2:
    player = steamChamber.getPlayer(steamid)

    print "<div style=\"padding: 10px\">"
    print "<pre>"

    print "player:       <b>" + player["personaname"].encode('utf-8') + "</b>"
    print "subscription: <b>" + player["subscription"] + "</b>"
    if player["subscription"] == "inactive-free":
        print "information:  <b>subscription is inactive</b>"
    if "pause" in player["subscription"]:
        print "information:  <b>subscription is paused</b>"
    print ""
    print "operations:"
    print "1. <a href=\"index.py?"+steamid+"-"+key+"-checksub\">check subscription status</a>"
    print "2. <a href=\"index.py?"+steamid+"-"+key+"-fetchstat\">manually fetch statistics</a> (max five times a day - operation takes up to one minute)"
    print "</pre>"
    print "</div>"

elif steamChamber.isValidRequestKey(steamid, key):
    if action == "checksub":
        playerData = steamChamber.requestPlayerProfileInfo(steamid)
        (subscription_valid, message) = steamChamber.isVerifiedSubscription(playerData)
        print "<div style=\"padding: 10px\">"
        print "<pre>"
        print message
        print "</pre>"
        print "</div>"

    elif action == "fetchstat":
        url_stat        = steamChamber.url_base_path + "rpc.py?method=GetUserStats&steamid="+steamid+"&key="+key
        #steamChamber.debug(url_stat); # nifty for debugging purposes

        returnObject    = urllib.urlopen(url_stat)
        return_json     = json.loads(returnObject.read())

        print "<div style=\"padding: 10px\">"
        print "<pre>"
        if "error" in return_json["response"]:
            print return_json["response"]["error"]
        if "final_status" in return_json["response"]:
            print return_json["response"]["final_status"]
        print "</pre>"
        print "</div>"

    else:
        print "unknown action"

    print "<div style=\"padding-left: 20px;\"><button onclick=\"javascript: window.history.go(-1);\">go back</button></div>"

else:
    print "fire in the hole!"

print """
</body>
</html>
"""
