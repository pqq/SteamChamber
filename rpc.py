
# --------------------------------------------------------------------------
# file          : rpc.py
# author        : Frode Klevstul ( frode at klevstul dot com)
# started       : march 2015
#
# * * * * * i'm a zoo in the jungle so python is right up my alley * * * * *
# --------------------------------------------------------------------------


import  cgitb
cgitb.enable()
import  json
import  cgi
import  steamChamber


def requestAndOutputData(method, steamid, key):
    """
    request data, and output result
    """
    url_steam           = ""
    path_local          = ""
    return_json         = ""
    return_text         = ""
    hidden_fields       = []

    # get profile info (name, avatar, etc) for given player
    if method == "GetPlayerSummaries":
        return_json = steamChamber.requestGetPlayerSummaries(steamid)

    # get csgo schema
    elif method == "GetItemsSchema":
        return_json = steamChamber.requestGetItemsSchema()

    # get csgo news
    elif method == "GetCSNews":
        return_json = steamChamber.requestGetNewsForApp()

    # list all players
    elif method == "GetPlayers":
        return_json = steamChamber.getPlayers()
        hidden_fields = ["email", "subscription", "players_inactive"]

    # return player statistics as json
    elif method == "GetStatForPlayerAsJson":
        return_json = steamChamber.getStatForPlayerAsJson(steamid)

    # return player statistics as csv
    elif method == "GetStatForPlayerAsCsv":
        return_text = steamChamber.getStatForPlayerAsCsv(steamid)

    # return player's extras (extra statistics)
    elif method == "GetExtrasForPlayer":
        return_json = steamChamber.getExtrasForPlayer(steamid)

    # get steamchamber subscriber status, plus subscription type, for a player
    elif method == "GetPlayerSteamchamberStatus":
        return_json = steamChamber.getPlayerSteamchamberStatus(steamid)

    # get random quote
    elif method == "GetQuote":
        return_json = steamChamber.getQuote()

    # get list of steam friends
    elif method == "GetFriendList":
        return_json = steamChamber.requestGetFriendList(steamid)

    # get user statistics
    elif method == "GetUserStats":
        return_json = steamChamber.requestGetUserStats(steamid, key)

    # wall of awesomeness
    elif method == "GetWallOfAwesomeness":
        return_json = steamChamber.getWallOfAwesomeness()

    print 'Content-Type: application/json; charset="utf-8"'
    print ""

    if (return_json):
        # hide fields from json output (sc-212)
        return_json = steamChamber.removeElementsFromDataStructure(return_json, hidden_fields)

        # make sure we always return data in a "response" object (sc-132)
        # check that the first key is not a "response" - "if not "response" in return_json:" would have checked all elements
        # ref: http://stackoverflow.com/questions/3097866/python-access-to-first-element-in-dictionary
        if not "response" in return_json.keys()[0]:
            return_json = {"response": return_json}

        print json.dumps(
              return_json
            , encoding      = "utf-8"
            , indent        = 4
        )
    elif (return_text):
        print return_text


def getCgiParameters():
    """
    get parameters sent along with web request
    """
    form = cgi.FieldStorage()
    parameters =(
        {
              "method"      : form.getvalue("method")
            , "steamid"     : form.getvalue("steamid")
            , "key"         : form.getvalue("key")
        }
    )

    #nifty for running on command line (debugging):
    #parameters =(
    #    {
    #          "method"      : "GetUserStats"
    #        , "steamid"     : "76561198052092770"
    #        , "key"         : "dda36ecf501c11e58837448a5b25560a"
    #    }
    #)

    return parameters


def main():
    params = getCgiParameters()
    requestAndOutputData(
          params["method"]
        , params["steamid"]
        , params["key"]
    )


if "__main__" == __name__:
    main()
